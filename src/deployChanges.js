import dotenv from "dotenv";
import path from "path";
import shell from "shelljs";

dotenv.config();

if (process.env.FOUNDRY_DATA_PATH && process.env.PROJECT_ROOT) {

  console.log(`FOUNDRY_DATA_PATH: ${process.env.FOUNDRY_DATA_PATH}`);
  console.log(`PROJECT_ROOT: ${process.env.PROJECT_ROOT}`);

  const modulePath = path.join(
    process.env.FOUNDRY_DATA_PATH,
    "Data",
    "modules",
    "danddcompendia"
  );

  console.log(`Deleting contents of directory: ${modulePath}`);
  shell.rm("-rf", `${modulePath}/*`);

  console.log(`Copying from:${process.env.PROJECT_ROOT}/dist/ to ${modulePath}`);
  shell.cp("-R", `${process.env.PROJECT_ROOT}/dist/*`, modulePath);
}
else{
  console.error("You must have both FOUNDRY_DATA_PATH and PROJECT_ROOT defined in your .env file.")
}
