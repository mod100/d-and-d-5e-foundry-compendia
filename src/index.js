import {extractPack, compilePack} from "@foundryvtt/foundryvtt-cli"

/**
 * "Actor"|"Adventure"|"Cards"|"ChatMessage"|"Combat"|"FogExploration"|"Folder"|"Item"|"JournalEntry"|"Macro"|
 *   "Playlist"|"RollTable"|"Scene"|"Setting"|"User"
 * } DocumentType
 */

const yaml = true;

const dbs = [
    '5eClasses.db',
    '5eFeats.db',
    '5eFeatures.db',
    '5eRaces.db',
    'CC.db',
    'class-features.db',
    'conditions.db',
    'FMP1.db',
    'GOS.db',
    'MM.db',
    'MPMM.db',
    'non-srd-items.db',
    'non-srd-spells.db',
    'non-srd-weapons.db',
    'POTA.db',
    'ToB.db',
    'ToB2.db',
    'VD.db',
    'PHB_Spells.db',
    'XGE_Spells.db',
    'TCE_Spells.db'
]

const documentTypeMap = {
    '5eClasses.db': 'Item',
    '5eFeats.db': 'Item',
    '5eFeatures.db': 'Item',
    '5eRaces.db': 'Item',
    'CC.db': 'Actor',
    'class-features.db': 'Item',
    'conditions.db': 'Item',
    'FMP1.db': 'Actor',
    'GOS.db': 'Actor',
    'MM.db': 'Actor',
    'MPMM.db': 'Actor',
    'non-srd-items.db': 'Item',
    'non-srd-spells.db': 'Item',
    'non-srd-weapons.db': 'Item',
    'POTA.db': 'Actor',
    'ToB.db': 'Actor',
    'ToB2.db': 'Actor',
    'VD.db': 'Actor',
    'PHB_Spells.db': 'Item',
    'XGE_Spells.db': 'Item',
    'TCE_Spells.db': 'Item'
}

dbs.forEach(async(db) => {

    console.log(`Processing ${db}`)
    const tempPath = `C:\\Users\\Matt Duffy\\Documents\\repos\\danddcompendia\\src\\tmp\\${db.substring(0, db.length-3)}`
    const dbPath = `C:\\Users\\Matt Duffy\\Documents\\repos\\danddcompendia\\src\\packs\\${db}`
    const newDbPath = `C:\\Users\\Matt Duffy\\Documents\\repos\\danddcompendia\\src\\leveldbpacks\\${db.substring(0, db.length-3)}`

    await extractPack(dbPath, tempPath, {yaml, transformName, nedb: true, documentType: documentTypeMap[db]})
    await compilePack(tempPath,newDbPath, {yaml})
})
 
function transformName(doc) {
    const safeFileName = doc.name.replace(/[^a-zA-Z0-9А-я]/g, "_");
    const type = doc._key.split("!")[1];
    const prefix = ["actors", "items"].includes(type) ? doc.type : type;
  
    return `${doc.name ? `${prefix}_${safeFileName}_${doc._id}` : doc._id}.${
      yaml ? "yml" : "json"
    }`;
}