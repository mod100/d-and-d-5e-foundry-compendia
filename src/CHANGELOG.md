# Changelog

## 3.0.0.0

Huge changes to use monsters and spells imported from 5eTools.

## 2.2.0.9

Set versatile damage correctly on weapons.

## 2.2.0.8

Fix token scaling.

## 2.2.0.7

Modified type in pack definitions to be correct.

## 2.2.0.6

Added v11 support.

## 2.2.0.5

Added vision to all tokens and modified data model to match the data model
for tokens in Foundry V10.

## 2.2.0.4

Fixed the image mapping for spells whose icons have moved in the system

## 2.2.0.3

Updated the compendia for non monsters to the newer Foundry data types
i.e. system instead of data

## 2.2.0.2

Changed core versions to indicate compatible with Foundry v10

## 2.2.0.1

Full set of images for Ghosts of Saltmarsh, Princes of the Apocalypse,
Tome of Beasts, Tome of Beasts 2 and Creature Codex

## 2.2.0.0

Full set of images for actors in the Monster Manual.

## 2.1.0.9

Added CHANGELOG.md for Module Management+