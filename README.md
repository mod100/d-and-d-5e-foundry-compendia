# CONFIGURATION

Create a .env file in the root of the project, it needs 2 settings if you want
to be able to deploy changes:

```bash
FOUNDRY_DATA_PATH=<path to foundry data root>
PROJECT_ROOT=<path to root directory of the project>
```
